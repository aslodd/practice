namespace MyLib{
class Core
{
   public int score = 0;
   private int win = 6, draw = 3, lose = 0, rock = 1, paper = 2, scissors = 3;
   public void game(char first_person, char second_person){
        if (first_person == 'A' && second_person == 'Y'){
           score = score + win + paper; 
        } else if (first_person == 'A' && second_person == 'X') {
            score = score + draw + rock;
        } else if (first_person == 'A' && second_person == 'Z') {
            score = score + lose + scissors;
        } else if (first_person == 'B' && second_person == 'Y') {
            score = score + draw + paper;
        } else if (first_person == 'B' && second_person == 'X') {
            score = score + lose + rock;
        } else if (first_person == 'B' && second_person == 'Z') {
            score = score + win + scissors;
        } else if (first_person == 'C' && second_person == 'Y') {
            score = score + lose + paper;
        } else if (first_person == 'C' && second_person == 'X') {
            score = score + win + rock;
        } else if (first_person == 'C' && second_person == 'Z') {
            score = score + draw + scissors;
        }                   
    }
   }
}
