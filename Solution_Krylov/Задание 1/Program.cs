﻿Console.WriteLine("Сколько раундов длится игра: ");
int user_count = 0;
int enemy_count = 0;
List<char> enemy_value = new List<char>() { 'A', 'B', 'C' };
int rounds = Convert.ToInt32(Console.ReadLine());
while (rounds > 0){
    Random rnd = new Random();
    int randIndex = rnd.Next(enemy_value.Count);
    char random_enemy_value = enemy_value[randIndex];
    Console.WriteLine(random_enemy_value);
    if (random_enemy_value == 'A'){
        user_count = user_count + 8;
        enemy_count = enemy_count + 1; 
        Console.WriteLine("Вы победили!");
    } else if(random_enemy_value == 'B'){
        user_count = user_count + 1;
        enemy_count = enemy_count + 8;
        Console.WriteLine("Вы проиграли!");
    }else{
        enemy_count = enemy_count + 6;
        user_count = user_count + 6;
        Console.WriteLine("Ничья");
    }
    rounds = --rounds;
}  
Console.WriteLine ($"По итогу игр вы набрали {user_count} очка(ов)!");
Console.WriteLine ($"По итогу игр противник набрал {enemy_count} очка(ов)!");
if (user_count > enemy_count){
    Console.WriteLine ("Вы выйграли!");
}
else if(user_count < enemy_count){
   Console.WriteLine ("Вы проиграли!"); 
}
else{
    Console.WriteLine ("Ничья!");
}