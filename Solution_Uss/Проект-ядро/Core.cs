namespace Library {

using System.Collections.Generic;
using System.Linq;

public class Elves{

    List<List<int>> start_list = new List<List<int>>();
    public void max_calories(){

        Console.WriteLine("Сколько всего эльфов?");
        int count_elves = Convert.ToInt32(Console.ReadLine());

        for (int i = 0; i < count_elves; i++){
            start_list.Add(new List<int>());
        }

        Console.WriteLine("Введите числа (для завершения введите пустую строку):");
        for (int i=0; i<count_elves; i++){
            while (true)
            {
                string input_calories = Console.ReadLine();

                if (string.IsNullOrEmpty(input_calories)){
                    break;
                }
                else{
                    int.TryParse(input_calories, out int calories);
                    start_list[i].Add(calories);
                }   
            }
        }

        List<int> sum_list = new List<int> ();

        for(int i=0; i<count_elves; i++){
            sum_list.Add(start_list[i].Sum());
        }

        int max = sum_list.Max();
        int number_elves = sum_list.LastIndexOf(sum_list.Max());
        Console.WriteLine($"Максимальное количество колорий - {max}, от {number_elves+1} эльфа");

    }
    /*
    public int q1,q2,q3,q4,q5 = 0;
    public int a1,a2,a3,a4,a5 = 0;
    public void max_calories(){

        Console.WriteLine("Сколько фруктов несет первый эльф?");
        q1 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Сколько фруктов несет второй эльф?");
        q2 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Сколько фруктов несет третий эльф?");
        q3 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Сколько фруктов несет четвертый эльф?");
        q4 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Сколько фруктов несет пятый эльф?");
        q5 = Convert.ToInt32(Console.ReadLine());

        List<int> elve1 = new List<int> (q1);  
        List<int> elve2 = new List<int> (q2);  
        List<int> elve3 = new List<int> (q3);  
        List<int> elve4 = new List<int> (q4);  
        List<int> elve5 = new List<int> (q5);  

        Console.WriteLine($"Введите колории {q1} фруктов первого эльфа");
        for (int i = 0; i < q1; i++){
            a1 = Convert.ToInt32(Console.ReadLine());
            elve1.Add(a1);
        }

        Console.WriteLine($"Введите колории {q2} фруктов второго эльфа");
        for (int i = 0; i < q2; i++){
            a2 = Convert.ToInt32(Console.ReadLine());
            elve2.Add(a2);
        }

        Console.WriteLine($"Введите колории {q3} фруктов третьего эльфа");
        for (int i = 0; i < q3; i++){
            a3 = Convert.ToInt32(Console.ReadLine());
            elve3.Add(a3);
        }

        Console.WriteLine($"Введите колории {q4} фруктов четвертого эльфа");
        for (int i = 0; i < q4; i++){
            a4 = Convert.ToInt32(Console.ReadLine());
            elve4.Add(a4);
        }

        Console.WriteLine($"Введите колории {q5} фруктов пятого эльфа");
        for (int i = 0; i < q5; i++){
            a5 = Convert.ToInt32(Console.ReadLine());
            elve5.Add(a5);
        }

        int sum1 = elve1.Sum();
        int sum2 = elve2.Sum();
        int sum3 = elve3.Sum();
        int sum4 = elve4.Sum();
        int sum5 = elve5.Sum();

        List<int> sum_final = new List<int> (){sum1,sum2,sum3,sum4,sum5};  

        int max = sum_final.Max();
        int ind = sum_final.LastIndexOf(sum_final.Max());
        Console.WriteLine($"Максимальное количество колорий - {max}, от {ind+1} эльфа");
    } Крылов
    */

}
}